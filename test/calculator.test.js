const Calculator = require('../dist/calculator').Calculator;
const assert = require('assert')

// Question 1 Tests
describe('Question 1', () => {
    it('should add "1,2,5" up to 8', () => {
        assert.strictEqual(Calculator.Add("1,2,5"), 8);
        assert.strictEqual(Calculator.Add("1 , 2,  5  "), 8);
        assert.strictEqual(Calculator.Add("1,2,,5"), 8);
        assert.strictEqual(Calculator.Add("1,2,5,"), 8);
    })

    it('should add "" up to 0', () => {
        assert.strictEqual(Calculator.Add(""), 0);
        assert.strictEqual(Calculator.Add("    "), 0);
    })
});

describe('Question 2', () => {
    it('should support newlines', () => {
        assert.strictEqual(Calculator.Add("1\n,2,3"), 6);
        assert.strictEqual(Calculator.Add("\n10,20,30"), 60);
        assert.strictEqual(Calculator.Add("\n2\n,4,6"), 12);
        assert.strictEqual(Calculator.Add("\n20,\n40\n,\n60\n"), 120);
    })
});

describe('Question 3', () => {
    it('should support custom delimiters', () => {
        assert.strictEqual(Calculator.Add("//;\n1;3;4"), 8);
        assert.strictEqual(Calculator.Add("//@\n2@4@6@10"), 22);
        assert.strictEqual(Calculator.Add("///\n5/4\n/3"), 12);
    })
});

describe('Question 4', () => {
    it('should not support negatives', () => {
        try {
            Calculator.Add("1,-2,3");
        } catch (err) {
            assert(err instanceof Error);
            assert.strictEqual(err.message, "Negatives not allowed: -2");
        }

        try {
            Calculator.Add("1,-2,-3");
        } catch (err) {
            assert(err instanceof Error);
            assert.strictEqual(err.message, "Negatives not allowed: -2,-3");
        }
    })
});

describe('Bonus 1', () => {
    it('should ignore numbers over 1000', () => {
        assert.strictEqual(Calculator.Add("2, 1000"), 1002);
        assert.strictEqual(Calculator.Add("2, 1001"), 2);
    })
});

describe('Bonus 2', () => {
    it('should support delimiters of arbitrary length', () => {
        assert.strictEqual(Calculator.Add("//***\n1***2***3"), 6);
        assert.strictEqual(Calculator.Add("//????\n1????2????3"), 6);
    })
});

describe('Bonus 3', () => {
    it('should support multiple delimiters', () => {
        assert.strictEqual(Calculator.Add("//$,@\n1$2@3"), 6);
        assert.strictEqual(Calculator.Add("//(,),[,]\n(1)(2)[3]"), 6);
    })
});

describe('Bonus 4', () => {
    it('should support multiple delimiters of arbitrary length', () => {
        assert.strictEqual(Calculator.Add("//$$$,@@\n1$$$2@@3@@4"), 10);
    })
});

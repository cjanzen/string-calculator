import { Calculator } from "./calculator";
import readline from 'readline';

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

class App {

    public static main() {
        rl.question('Enter a list of numbers: ', ans => {
            console.log(`${ans} = ${Calculator.Add(ans)}`);

            rl.close();
        });
    }

}

App.main();

export class Calculator {
    /**
     * We will use this to get a string of delimiters to use to
     * split the string using a regular expression
     * 
     * @param dels The array of strings containing the
     * custom delimiters
     */
    private static toRegex(dels: string[]) {
        // This replaces all of the commas with |s for the regex
        const strs = dels.map(val => `\\${val}`).join("|");
        return new RegExp(`[^${strs}]+`, "gm");
    }

    /**
     * Adds a bunch of numbers contain in the given
     * comma-delimited string.
     * 
     * @param numbers - a string consisting of numbers delimited
     * by one or more delimiters provided by the user ("," by default)
     */
    public static Add(numbers: string): number {
        // A list of numbers that are erronous
        let errors: number[] = [];
        // The sum we will be returning
        let sum = 0;
        // The custom delimiter ("," by default)
        let delimiters = this.toRegex([","]);
        // The list itself (w/o control code)
        let list = "";

        // Step 1: Grab the list from the string
        const matches = numbers.match(/^(?:\/\/(.*)\n)?([0-9\n\W]*)/);
        list = matches[2];

        // Step 2: Grab the custom delimiters (if any)
        if ( matches[1] )
            delimiters = this.toRegex(matches[1].match(/[^,]+/g));

        // Step 3: Find all of the numbers in the string using regex.

        /// Questions 1 & 2
        /// numbers.match(/\d+/gm).forEach( value => sum += parseInt(value));

        /// Question 3
        if (delimiters.test(list)) {
            list.match(delimiters).forEach( value => {
                // Step 4: Add each number to the sum
                let val = parseInt(value); 
                // If the number is negative, add number to error list
                if ( val < 0 ) errors.push(val);
                // Otherwise, if the number is larger than 1000, ignore it
                else if ( val > 1000 || isNaN(val) ) val = 0;
    
                // Add the value to the sum
                sum += val;
            });
        }

        // If there were any negatives, show them
        if ( errors.length > 0 ) {
            throw new Error(`Negatives not allowed: ${errors}`)
        }
        

        // Step 5: Return the sum
        return sum;
    }
}


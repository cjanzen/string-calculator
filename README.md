# String Calculator

A simple program written for a technical interview. Takes in a comma-seperated (or any custom delimiter in the format ``//;,&\n``, where ``;`` and ``&`` are the delimiters) list of numbers and adds them together, with a few rules:
- Numbers can't be negative
- Numbers over 1000 will be ignored

For example, if you enter a string with this ``1,2,3`` or ``//;\n1;2;3``, you would get ``6``.

This program was written in **TypeScript**.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for testing purposes.

### Prerequisites

- **Node JS**: The JavaScript runtime engine used to run the program. You can download it from https://nodejs.org/en/download/.

### Installing
1. Open a terminal/command prompt in the root of the project directory.
2. Run ``npm install`` to get all of the dependencies.

### Usage

- ``npm run build``: Just builds the project into the ``dist`` directory in the root of the project.
- ``npm run run``: Builds and runs the project, which will prompt you to enter a string
- ``npm run test``: Runs through all of the unit tests to ensure that the Calculator class is working.

## Built With

* [NodeJS](https://nodejs.org) - The JavaScript runtime engine used to run the program.
* [TypeScript](https://www.typescriptlang.org) - The language used to develop the program (compiles to JavaScript).
* [Mocha](https://mochajs.org) - Test Framework for JavaScript

## Authors

* **Corey Janzen**  - [cjanzen](https://bitbucket.org/cjanzen/)

